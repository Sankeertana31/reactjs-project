//import express module
let express=require("express");
//import body-parser module
let bodyparser=require("body-parser");
//create the rest obj
let app=express();
//enable the ports communication 
let cors = require("cors");
//set the JSON as mime type
app.use(express.json())
app.use(bodyparser.json());
//read json
app.use(bodyparser.urlencoded({extended:false}));
app.use(cors());
//use login module
app.use("/login",require("./login/login"));
//use register module
app.use("/register",require("./register/register"));
//use update module 
  //for personal details
app.use("/update",require("./update/update"));
  //for educational details
app.use("/updateEdu",require("./update/updateEdu"));
//for project details
app.use("/updateProj",require("./update/updateProj"));
//for skills details
app.use("/updateSkills",require("./update/updateSkills"));
//for experiences details
app.use("/updateExp",require("./update/updateExp"));

app.use("/delete",require("./delete/delete"));

app.use("/fetch",require("./fetch/fetch"));
app.listen(3000);
console.log("server listening the port no.3000");
