import logo from './logo.svg';
import './App.css';
import HomePage from './HomePage';
import Login from './Login';
import Register from './Register';
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { ProfilePage } from './ProfilePage';
function App() {
  return (
    /*<div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
      <HomePage />
    </div>*/
    <div className="App">
        <Routes>
          <Route path="/" element={<HomePage />} />
          <Route path="/login" element={<Login />} />
          <Route path="/register" element={<Register />} /> 
          <Route path="/profilepage" element={<ProfilePage />} />
        </Routes>
      {/*<HomePage />*/}
    </div>
  );
}

export default App;
