import React, {PureComponent} from 'react';
import jsPDF from 'jspdf'
import 'jspdf-autotable'
import PersonalDetails from './PersonalDetails';
import { useLocation } from 'react-router';
export const Pdfgenerator = () =>{
  const location = useLocation();
  const Data = location.state.data; 
  console.log(Data) 
  function jsPdfGenerator(){
    var doc= new jsPDF('p','pt');
    doc.setFont('courier');
    doc.rect(20,20,doc.internal.pageSize.width - 40,doc.internal.pageSize.height - 40,'S');
    doc.setFontSize(20);
    doc.text(40,40,Data.First_Name);
    doc.text(150,40,Data.Last_Name);
    doc.setFontSize(20);
    doc.text(380,40,"Contact");
    doc.setFontSize(10);
    doc.text(40,60,"Objective:");
    doc.text(110,60,"To secure a challenging possition in a");
    doc.text(40,80,"reputable organization to expand my learnings,");
    doc.text(40,100,"knowledge and skills");
    doc.text(380,60,Data.Email_Id);
    doc.text(380,80,Data.MobileNo);
    doc.text(380,100,Data.Github);
    doc.text(380,120,Data.LinkedIn);
    doc.setFontSize(20);
    doc.text(40,140,"Educational Details");
    doc.setFontSize(10);
    doc.autoTable({ html: '#my-table' })
    doc.autoTable({
      startY:160,
      theme:'grid',
      head: [['Name of Insitution','Specialization','Year of Passing','Marks']],
      body: [
        [Data.sname ,Data.Squalification,Data.SYOF,Data.Smarks],
        [Data.cname ,Data.Cqualification,Data.CYOF,Data.Cmarks],
        [Data.uname ,Data.Uqualification,Data.UYOF,Data.Umarks],
        [Data.pname ,Data.Pqualification,Data.PYOF,Data.Pmarks],
      ],
    })
    doc.setFontSize(20);
    doc.text(40,290,"Projects Developed")
    doc.setFontSize(10);
    doc.text(40,310,"Project 1:")
    doc.text(60,330,"Title:")
    doc.text(100,330,Data.title1);
    doc.text(60,350,"Link:")
    doc.text(100,350,Data.link1);
    doc.text(60,370,"Description:")
    doc.text(130,370,Data.description1);
    doc.text(40,390,"Project 2:")
    doc.text(60,410,"Title:")
    doc.text(100,410,Data.title2);
    doc.text(60,430,"Link:")
    doc.text(100,430,Data.link2);
    doc.text(60,450,"Description:")
    doc.text(130,450,Data.description2);
    doc.text(40,470,"Project 3:")
    doc.text(60,490,"Title:")
    doc.text(100,490,Data.title3);
    doc.text(60,510,"Link:")
    doc.text(100,510,Data.link3);
    doc.text(60,530,"Description:")
    doc.text(130,530,Data.description3);
    doc.setFontSize(20);
    doc.text(40,550,"Skills/Languages")
    doc.setFontSize(10);
    doc.text(40,570,Data.skill1);
    doc.text(40,590,Data.skill2);
    doc.text(40,610,Data.skill3);
    doc.text(40,630,Data.skill4);
    doc.setFontSize(20);
    doc.text(40,650,"Experience/Internships")
    doc.setFontSize(10);
    doc.text(40,670,"Organization:")
    doc.text(120,670,Data.inst1);
    doc.text(40,690,"Duration:")
    doc.text(100,690,Data.dur1);
    doc.text(40,710,"Position:")
    doc.text(100,710,Data.pos1);
    doc.text(40,730,"Description:")
    doc.text(120,730,Data.desc1);
    doc.text(40,750,"Organization:")
    doc.text(120,750,Data.inst2);
    doc.text(40,770,"Duration:")
    doc.text(100,770,Data.dur2);
    doc.text(40,790,"Position:")
    doc.text(100,790,Data.pos2);
    doc.text(40,810,"Description:")
    doc.text(120,810,Data.desc2);
    doc.addPage('a4', 'portrait');
    doc.rect(20,20,doc.internal.pageSize.width - 40,doc.internal.pageSize.height - 40,'S');
    doc.text(40,40,"Organization:")
    doc.text(120,40,Data.inst3);
    doc.text(40,60,"Duration:")
    doc.text(100,60,Data.dur3);
    doc.text(40,80,"Position:")
    doc.text(100,80,Data.pos3);
    doc.text(40,100,"Description:")
    doc.text(120,100,Data.desc3);
    doc.save("resume.pdf");
  }
  return(<button className="button" onClick={jsPdfGenerator}>Download Resume</button>)
}
export default Pdfgenerator;


