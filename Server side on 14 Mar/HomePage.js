import React from 'react'
import { useNavigate } from "react-router-dom";
//import login from './Login'
//import register from './Register'
import {Navbar,Nav,Container,NavDropdown} from "react-bootstrap"
function HomePage(){
  const navigate = useNavigate();  
  return (
    <div className="homepage">
        {/*<h1>Welcome to HomePage!!</h1>
        <br />
        <button color="primary" onClick={() => navigate("/login")}>LOGIN</button>
        <br />
        <br />
        <br />
        <button color="primary" onClick={() => navigate("/register")}>REGISTER</button>*/}
        <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
  <Container>
  <Navbar.Brand href="home"></Navbar.Brand>
  <Navbar.Toggle aria-controls="responsive-navbar-nav" />
  <Navbar.Collapse id="responsive-navbar-nav">
    <Nav className="me-auto">
      <Nav.Link href="/#login"></Nav.Link>
      <Nav.Link href="/#register"></Nav.Link>
    </Nav>
    <Nav>
    <Nav.Link href="/login">LOGIN</Nav.Link>
      <Nav.Link href="/register">REGISTER</Nav.Link>
      </Nav>

  </Navbar.Collapse>
  </Container>
  </Navbar>
  </div>
  )
}
export default HomePage;
