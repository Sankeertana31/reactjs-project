import React, {PureComponent} from 'react';
import jsPDF from 'jspdf'
import PersonalDetails from './PersonalDetails';
import { useLocation } from 'react-router';
export const Pdfgenerator = () =>{
  const location = useLocation();
  const Data = location.state.data; 
  console.log(Data) 
  function jsPdfGenerator(){
    var doc= new jsPDF('p','pt');
    doc.setFont('courier');
    doc.text(20,20,Data.First_Name);
    doc.text(20,20,Data.Last_Name);
    doc.text(40,40,Data.Email_Id);
    doc.text(20,20,Data.MobileNo);
    doc.text(20,20,Data.Github);
    doc.text(20,20,Data.LinkedIn);
    doc.text(50,50,Data.CYOF);
    doc.text(20,20,Data.Cmarks);
    doc.text(20,20,Data.Cqualification);
    doc.text(20,20,Data.PYOF);
    doc.text(20,20,Data.Pmarks);
    doc.text(20,20,Data.Pqualification);
    doc.text(20,20,Data.SYOF);
    doc.text(20,20,Data.Smarks);
    doc.text(20,20,Data.Squalification);
    doc.text(20,20,Data.UYOF);
    doc.text(20,20,Data.Umarks);
    doc.text(20,20,Data.Uqualification);
    doc.text(20,20,Data.cname);
    doc.text(20,20,Data.pname);
    doc.text(20,20,Data.sname);
    doc.text(20,20,Data.uname);
    doc.text(20,20,Data.description1);
    doc.text(20,20,Data.description2);
    doc.text(20,20,Data.description3);
    doc.text(20,20,Data.link1);
    doc.text(20,20,Data.link2);
    doc.text(20,20,Data.link3);
    doc.text(20,20,Data.title1);
    doc.text(20,20,Data.title2);
    doc.text(20,20,Data.title3);
    doc.text(20,20,Data.skill1);
    doc.text(20,20,Data.skill2);
    doc.text(20,20,Data.skill3);
    doc.text(20,20,Data.skill4);
    doc.text(20,20,Data.desc1);
    doc.text(20,20,Data.desc2);
    doc.text(20,20,Data.desc3);
    doc.text(20,20,Data.dur1);
    doc.text(20,20,Data.dur2);
    doc.text(20,20,Data.dur3);
    doc.text(20,20,Data.inst1);
    doc.text(20,20,Data.inst2);
    doc.text(20,20,Data.inst3);
    doc.text(20,20,Data.pos1);
    doc.text(20,20,Data.pos2);
    doc.text(20,20,Data.pos3);
    doc.save("resume.pdf");
   
  }
    return(<button onClick={jsPdfGenerator}>Download Resume</button>)
}
export default Pdfgenerator;