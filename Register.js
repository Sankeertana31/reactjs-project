import React, {useState} from 'react';
function Register()
{
    const[First_Name,setFirst_Name]=useState("")
    const[Last_Name,setLast_Name]=useState("")
    const[Email_ID,setEmail_ID]=useState("")
    const[MobileNo,setMobileNo]=useState("")
    const[Password,setPassword]=useState("")
    const[Confirm_Password,setConfirm_Password]=useState("")
    async function signUp(){
        let item={First_Name,Last_Name,Email_ID,MobileNo,Password,Confirm_Password}
        console.warn(item)
        let result= await fetch("http://localhost:3000/register" ,{
            method:'POST',
            body:JSON.stringify(item),
            headers:{
                "Content-Type":'application/json',
                "Accept":'application/json'
            }

        })
        result = await result.json()
        console.warn("result",result)

    }
    return (
        <div className="col-sm-6 offset-sm-3">
        <h1> Registration Page </h1>
        <input type="text" value={First_Name} onChange={(e)=>setFirst_Name(e.target.value)} className="form-control" placeholder="First Name"/>
        <br/>
        <input type="text" value={Last_Name} onChange={(e)=>setLast_Name(e.target.value)}className="form-control" placeholder="Last Name"/>
        <br/>
        <input type="text" value={Email_ID} onChange={(e)=>setEmail_ID(e.target.value)} className="form-control" placeholder="EmailID"/>
        <br/>
        <input type="text" value={MobileNo} onChange={(e)=>setMobileNo(e.target.value)}className="form-control" placeholder="Mobile Number"/>
        <br/>
        <input type="password" value={Password} onChange={(e)=>setPassword(e.target.value)} className="form-control" placeholder="Password"/>
        <br/>
        <input type="password" value={Confirm_Password} onChange={(e)=>setConfirm_Password(e.target.value)}className="form-control" placeholder="Confirm Password"/>
        <br/>
        <button onClick={signUp} className="btn btn-primary">Register</button>
        </div>  
    )
}
export default Register;








/*import React, {useState} from 'react';
import { useNavigate } from "react-router-dom";
function Register(props) {
  const navigate = useNavigate();
  return(
        <div className="card col-12 col-lg-4 login-card mt-2 hv-center">
            <form>
                <div className="form-group text-left">
                <label htmlFor="exampleInputEmail1">Email address</label>
                <input type="email" 
                       className="form-control" 
                       id="email" 
                       aria-describedby="emailHelp" 
                       placeholder="Enter email"
                />
                <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
                </div>
                <div className="form-group text-left">
                    <label htmlFor="exampleInputPassword1">Password</label>
                    <input type="password" 
                        className="form-control" 
                        id="password" 
                        placeholder="Password"
                    />
                </div>
                <div className="form-group text-left">
                    <label htmlFor="exampleInputPassword1">Confirm Password</label>
                    <input type="password" 
                        className="form-control" 
                        id="confirmPassword" 
                        placeholder="Confirm Password"
                    />
                </div>
                <button 
                    type="submit" 
                    className="btn btn-primary"
                >
                    Register
                </button>
                <button onClick={() => navigate("/")}>Go Back</button>
            </form>
        </div>
    )
} 

export default Register;*/