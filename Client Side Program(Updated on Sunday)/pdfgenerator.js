import React, {PureComponent} from 'react';
import jsPDF from 'jspdf'
import PersonalDetails from './PersonalDetails';
export default class pdfGenerator extends PureComponent{
  constructor(props){
    console.log("props")
  }
  jsPdfGenerator=() => {
    var doc= new jsPDF('p','pt');
    doc.text(40,40,this.props.fname)
    doc.setFont('courier');
    doc.text(20,20,this.props.lname)
    doc.save("resume.pdf");

  }
  render(){
    return(<button onClick={this.jsPdfGenerator}>Download Resume</button>)
  }
}
