import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import FormValidate from "./FormValidate";
import { useLocation } from "react-router"; 


export const ConfirmPage = () => {
    const location = useLocation();
    const primarykey = location.state.email 
    console.log("primary key : ",primarykey)
    const [Data,setData] = useState("")
    useEffect(()=>{
        Confirm()
    })
    async function Confirm(){
        let result = await fetch("http://localhost:3000/fetch", {
            method: "POST",
            body: JSON.stringify({primarykey}),
            headers: {
              "Content-Type": "application/json",
              "Accept" : "application/json"
            }
          });
        if(result.status == 404){
           alert("cannot display data")
        }
        result = await result.json() 
        setData(result) 
    };
        
  return (
    <div>
        <h1>ConfirmPage</h1>
        <p>{Data}</p>
    </div>
  )
}
