import React from 'react'
import { useNavigate } from "react-router-dom";
//import login from './Login'
//import register from './Register'

function HomePage(){
  const navigate = useNavigate();  
  return (
    <div>
        <p>Welcome to HomePage!!</p>
        <br />
        <button color="primary" onClick={() => navigate("/login")}>LOGIN</button>
        <br />
        <br />
        <br />
        <button color="primary" onClick={() => navigate("/register")}>REGISTER</button>
    </div>
  )
}
export default HomePage;
