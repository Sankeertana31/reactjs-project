import React, { useState,callback } from 'react'
import {omit} from 'lodash'
//import { Navigate } from 'react-router';
import { useNavigate } from 'react-router-dom';

const FormValidate = () => {
    
    //Form values
    const [values, setValues] = useState({});
    //Errors
    const [errors, setErrors] = useState({});
    const navigate = useNavigate();
    
    const validate = (event, name, value) => {
        //A function to validate each input values

        switch (name) {
            case 'fname':
                if(value.length <= 4){
                    // we will set the error state

                    setErrors({
                        ...errors,
                        fname:'Username atleast have 5 letters'
                    })
                }else{
                    // set the error state empty or remove the error for username input

                    //omit function removes/omits the value from given object and returns a new object
                    let newObj = omit(errors, "fname");
                    setErrors(newObj);
                    
                }
                break;
            
                case 'lname':
                    if(value.length <= 4){
                        // we will set the error state
    
                        setErrors({
                            ...errors,
                            fname:'Username atleast have 5 letters'
                        })
                    }
                    else if(value.length >=20){
                        setErrors({
                            ...errors,
                            fname:'Username should have maximum 20 letters'
                        })
                    }
                        else{
                            // we will set the error state
    
                        setErrors({
                            ...errors,
                            fname:'Username atleast have 5 letters'
                        })
                    
                        // set the error state empty or remove the error for username input
    
                        //omit function removes/omits the value from given object and returns a new object
                        let newObj = omit(errors, "fname");
                        setErrors(newObj);
                        
                    }
                    break;    
                
            case 'email_id':
                if(
                    !new RegExp( /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/).test(value)
                ){
                    setErrors({
                        ...errors,
                        email:'Enter a valid email address'
                    })
                }else{

                    let newObj = omit(errors, "email_id");
                    setErrors(newObj);
                    
                }
            break;
            
            case "mobile":
                if(value.length < 10){
                    setErrors({
                        ...errors,
                        mobile:'mobile number should have atleast 10 digits'
                    })
                }else{
                    // set the error state empty or remove the error for username input

                    //omit function removes/omits the value from given object and returns a new object
                    let newObj = omit(errors, "fname");
                    setErrors(newObj);
                    
                }
            break;
            
            case 'linkedin':
                if(
                    !new RegExp(/^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/gm).test(value)
                ){
                    setErrors({
                        ...errors,
                        email:'Enter a valid linkedin url'
                    })
                }else{

                    let newObj = omit(errors, "linked");
                    setErrors(newObj);
                    
                }
            break;
        
            
            
            default:
                break;
        }
    }



  //A method to handle form inputs
    const handleChange = (event) => {
        //To stop default events    
        event.persist();

        let name = event.target.name;
        let val = event.target.value;
        console.log(name,val)
        validate(event,name,val);
        
        //Let's set these values in state
        setValues({
            ...values,
            [name]:val,
        })

    }


    const handleSubmit = (event) => {
        if(event) event.preventDefault();

        if(Object.keys(errors).length === 0 && Object.keys(values).length !==0 ){
            navigate(-1)

        }else{
            alert("There is an Error!");
        }
    }

    return {
        values,
        errors,
        handleChange,
        handleSubmit
    }
}

export default FormValidate;