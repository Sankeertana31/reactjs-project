import React,{  useState} from 'react'
import {Button} from 'reactstrap'
import { useNavigate } from 'react-router-dom'
import FormValidate from './FormValidate';

//import './ResumeForm/Educationaldetails'
export const PersonalDetails = () => {
     
  const checkForm = () => {

    console.log("Callback function when form is submitted!");
    console.log("Form Values ", values);
  }

    const navigate = useNavigate();
    const initialValues = {fname:"",lname:"",email_id:"",mobile:"",linkedin:"",github:""};
    const {handleSubmit,handleChange, values,errors } = FormValidate(checkForm);
    //values = useState(initialValues);
    //const [fname,setfname] = useState("");
    //const[lname,setlname]=useState("");
    //const[email_id,setemail_id]=useState("");
    //const[mobile,setmobileno] = useState("");
    //const[linkedin,setlinkedin] = useState("");
    //const[github,setgithub]=useState("");
  return (
    <div>
        <form onSubmit={handleSubmit}>
            <label>First Name:</label>
            <input type="text" name="fname" onChange={handleChange} className="form-control" required placeholder="First Name" />{
              errors.fname && <h5>{errors.fname}</h5>
            }
            <br/>
            <label>Last Name:</label>
            <input type="text" name="lname" onChange={handleChange} className="form-control" required placeholder="Last Name" />{
              errors.lname && <h5>{errors.lname}</h5>
            }
            <br/>
            <label>Email ID</label>
            <input type="text" name="email_id" onChange={handleChange} className="form-control" required placeholder="Email Id"/>{
              errors.email_id && <h5>{errors.email_id}</h5>
            }
            <br/>
            <label>Mobile No:</label>
            <input type="text" name="mobile" onChange={handleChange} className="form-control" required placeholder="Mobile Number"/>{
              errors.mobile && <h5>{errors.mobile}</h5>
            }
            <br/>
            <label>Linked In:</label>
            <input type="url" name="linkedin" onChange={handleChange} className="form-control" required placeholder="Linked URL"/>{
                  errors.linkedin && <h5>{errors.linkedin}</h5>
            }
            <br/>
            <label>Github:</label>
            <input type="url" name="github" onChange={handleChange} className="form-control" required placeholder="Github URL"/>{
                  errors.github && <h5>{errors.github}</h5>
            }
            <br/>
            {/*<input type="submit" value="Submit" />*/}
            <button type="submit" onClick={() => navigate(-1)}>BACK</button> 
            <br/> 
            <button type="submit" onClick={() => navigate("/ResumeForm/Educationaldetails")}>NEXT</button>
            {/*<p>{initialValues.fname},{initialValues.fname},{initialValues.fname},{initialValues.fname},{initialValues.fname},{initialValues.fname}</p>*/}
        </form>
    </div>
  )
}
export default PersonalDetails;
