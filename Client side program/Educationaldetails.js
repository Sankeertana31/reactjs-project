import React from 'react'
import { useNavigate } from 'react-router-dom'
import FormValidate from './FormValidate'
export const Educationaldetails = () => {
    const checkForm = () => {

        console.log("Callback function when form is submitted!");
        console.log("Form Values ", values);
      }
    const navigate = useNavigate();
    const initialValues = {UDetails:{uname:"",UYOF:new Date(),Uqualification:"",Umarks:""},Icollege:{cname:"",CYOF:new Date(),Cqualification:"",Cmarks:""},School:{sname:"",SYOF:new Date(),Squalification:"",Smarks:""}};
    const {handleSubmit,handleChange, values,errors } = FormValidate(checkForm);

  return (

    <div>
        <h1>Educationaldetails</h1>
        <form onSubmit={handleSubmit}>
            <h1>UNIVERSITY DETAILS:</h1>
            <label>name of University/college </label>
            <input type="text" name="uname" onChange={handleChange} className="form-control" required/>
            <br/>
            <label>Year of passing            </label>
            <input type="Date" name="YOF" onChange={handleChange} className="form-control" required/>
            <br/>
            <label>qualification   </label>           
            <input type="text" name="qualification" onChange={handleChange} className="form-control" required/>
            <br/>
            <label>Marks(CGPA/Percentage)     </label>
            <input type="text" name="marks" onChange={handleChange} className="form-control" required/>
            <br/>
            {/*<input type="submit" value="Submit" />*/}

        </form>

        <br/><br/><br/>

        <form onSubmit={handleSubmit}>
            <h1>CLASS XII:</h1>
            <label>name of college        </label>
            <input type="text" name="cname" onChange={handleChange} className="form-control" required/>
            <br/>
            <label>Year of passing        </label>
            <input type="Date" name="YOF" onChange={handleChange} className="form-control" required/>
            <br/>
            <label>qualification          </label>
            <input type="text" name="qualification" onChange={handleChange} className="form-control" required/>
            <br/>
            <label>Marks(CGPA/Percentage) </label>
            <input type="text" name="marks" onChange={handleChange} className="form-control" required/>
            <br/>
            {/*<input type="submit" value="Submit" />*/}
        </form>
        <form onSubmit={handleSubmit}>
            <h1>CLASS X:</h1>
            <label>name of school           </label>
            <input type="text" name="sname" onChange={handleChange} className="form-control" required/>
            <br/>
            <label>Year of passing          </label>
            <input type="Date" name="YOF" onChange={handleChange} className="form-control" required/>
            <br/>
            <label>qualification            </label>
            <input type="text" name="qualification" onChange={handleChange} className="form-control" required/>
            <br/>
            <label>Marks(CGPA/percentage)   </label>
            <input type="text" name="marks" onChange={handleChange} className="form-control" required/>
            <button type="submit" onClick={() => navigate("-1")}>BACK</button>
            <br />
            <button type="submit" onClick={() => navigate("/ResumeForm/ProjectsDeveloped")}>NEXT</button>
            {/*<input type="submit" value="Submit" />*/}
        </form>
    </div>
  )
}
export default Educationaldetails;