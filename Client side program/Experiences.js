import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
export const Experiences = () => {
  const [skill1, setskill1] = useState("");
  const [skill2, setskill2] = useState("");
  const [skill3, setSkill3] = useState("");
  const [skill4, setskill4] = useState("");
  const navigate = useNavigate();
  async function experience() {
    let item = {
      skill1,
      skill2,
      skill3,
      skill4
    };
    console.warn(item);
    let result = await fetch("http://localhost:3000/register", {
      method: "POST",
      body: JSON.stringify(item),
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json"
      }
    });
    result = await result.json();
    console.warn("result", result);
  }
  return (
    <div className="col-sm-6 offset-sm-3">
      <h1> Skills and languages </h1>
      <br />
      <input
        type="text"
        value={skill1}
        onChange={(e) => setskill1(e.target.value)}
        className="form-control"
        placeholder="Skill 1"
      />
      <br />
      <input
        type="text"
        value={skill2}
        onChange={(e) => setskill2(e.target.value)}
        className="form-control"
        placeholder="Skill 2"
      />
      <br />
      <input
        type="text"
        value={skill3}
        onChange={(e) => setSkill3(e.target.value)}
        className="form-control"
        placeholder="Skill 3 "
      />
      <br />
      <input
        type="text"
        value={skill4}
        onChange={(e) => setskill4(e.target.value)}
        className="form-control"
        placeholder="Skill 4"
      />
      <br />
      <br />
      <br />
    </div>
  );
}
export default Experiences;