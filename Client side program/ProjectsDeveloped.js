import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
export const ProjectsDeveloped = () => {
  const [Title, setTitle] = useState("");
  const [Link, setLink] = useState("");
  const [Description, setDescription] = useState("");
  const navigate = useNavigate();
  async function signUp() {
    let item = {
      Title,
      Link,
      Description
    };
    console.warn(item);
    let result = await fetch("http://localhost:3000/register", {
      method: "POST",
      body: JSON.stringify(item),
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json"
      }
    });
    result = await result.json();
    console.warn("result", result);
  }
  return (
    <div className="col-sm-6 offset-sm-3">
      <h1> Projects Developed </h1>
      <h2> Project 1 </h2>
      <input
        type="text"
        value={Title}
        onChange={(e) => setTitle(e.target.value)}
        className="form-control"
        placeholder="Title"
      />
      <br />
      <input
        type="text"
        value={Link}
        onChange={(e) => setLink(e.target.value)}
        className="form-control"
        placeholder="Link"
      />
      <br />
      <input
        type="text"
        value={Description}
        onChange={(e) => setDescription(e.target.value)}
        className="form-control"
        placeholder="Description"
      />
      <h2> Project 2 </h2>
      <input
        type="text"
        value={Title}
        onChange={(e) => setTitle(e.target.value)}
        className="form-control"
        placeholder="Title"
      />
      <br />
      <input
        type="text"
        value={Link}
        onChange={(e) => setLink(e.target.value)}
        className="form-control"
        placeholder="Link"
      />
      <br />
      <input
        type="text"
        value={Description}
        onChange={(e) => setDescription(e.target.value)}
        className="form-control"
        placeholder="Description"
      />

      <h2> Project 3 </h2>
      <input
        type="text"
        value={Title}
        onChange={(e) => setTitle(e.target.value)}
        className="form-control"
        placeholder="Title"
      />
      <br />
      <input
        type="text"
        value={Link}
        onChange={(e) => setLink(e.target.value)}
        className="form-control"
        placeholder="Link"
      />
      <br />
      <input
        type="text"
        value={Description}
        onChange={(e) => setDescription(e.target.value)}
        className="form-control"
        placeholder="Description"
      />
      <br />
      <br />
      <button className="btn btn-primary" onClick={()=>navigate("-1")}>
        Back
      </button>
      <br/>
      <button className="btn btn-primary" onClick={()=>navigate("/ResumeForm/ExtraDetails")}>
        Next
      </button>
    </div>
  );
}
export default ProjectsDeveloped;