import React, {useState} from 'react';
import { useNavigate } from "react-router-dom";
function Login()
{
    
    const[Email_ID,setEmail_ID]=useState("")
    const[Password,setPassword]=useState("")
    const navigate = useNavigate(); 
    async function signIn(){
        let item={Email_ID,Password}
        console.warn(item)
        try{
        let result= await fetch("http://localhost:3000/login/"+Email_ID+"/"+Password,{
            method:'GET',
            headers:{
                "Content-Type":'application/json',
                "Accept":'application/json'
            },
            //body:JSON.stringify(item)
        });
        console.log(result);
        if(result == null){
            console.log("invaild") 
            navigate("/")
        }
        console.log(result)
        localStorage.setItem("user-info",JSON.stringify(result));
        //result = JSON.parse(result)
        console.warn("result",result)
        navigate("/profilepage")
        //result = json.parse(result)
    }catch (err) {
        console.error('err', err);
      }
    }
    return (
        <div className="col-sm-6 offset-sm-3">
        <h1> Login Page </h1>
        <input type="text" value={Email_ID} onChange={(e)=>setEmail_ID(e.target.value)} className="form-control" placeholder="EmailID"/>
        <br/>
        <input type="password" value={Password} onChange={(e)=>setPassword(e.target.value)} className="form-control" placeholder="Password"/>
        <br/>
        <button onClick={signIn} className="btn btn-primary">Login</button>
        <button onClick={() => navigate(-1)}>Go Back</button>
        </div>  
    )
}
export default Login;


